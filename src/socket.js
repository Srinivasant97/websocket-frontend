function socketConnection(){
    const socket = new WebSocket(process.env.WEBSOCKET_URL);
    socket.onopen = function (e){
      console.log(e,new Date())
    }
    socket.onclose = function (e){
      console.log(e,new Date())
      setTimeout(socketConnection(),5000)
    }
  }
  socketConnection()